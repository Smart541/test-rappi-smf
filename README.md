# Prueba Rappi: The Movie DB

Por Sandro Martínez Felli.

## Arquitectura

La app fue desarrollada utilizando una arquitectura MvP en la que cada modulo utiliza un patrón compuesto por un Fragment o Activity con su xml correspondiente como vista, un presenter con el cual se comunica y uno o mas modelos.

La data de The Movie Database se obtiene bien sea de su Api utilizando Retrofit 2 como libreria para consultas Http o de una base de datos interna manejada con el ORM Realm.

## Capas de la aplicación: Modelos

Definen la data a ser recibida por la Api asi como también la estructura de las tablas para la Base de Datos interna. En la estructura del proyecto, estos se encuentran dentro del paquete "modelos". Solo los modelos asociados a una tabla de Realm seran persistidos en memoria para ser accedidos al no tener conexion.

Modelos no asociados a una tabla de Realm:

```
GenreListing: Objeto general que define la respuesta a la petición de Generos a la api. Contiene un listado de "Genre".
MovieListing: Objeto general que define la respuesta a la petición de Películas a la api. Contiene un listado de "MovieResult".
SeriesListing: Objeto general que define la respuesta a la petición de Series a la api. Contiene un listado de "SeriesResult".
VideoResult: Objeto general que contiene la lista de "Videos" relacionados a un "MovieDetails" o "SerieDetails".
Videos: Objeto que contiene la información de un video específico de una película o serie.
```

Modelos asociados a tablas de Realm:

```
Genre: Contiene la información de cada genero, tanto de Series como de Películas.
MovieDetails: Contiene los detalles de una película.
MovieResult: Contiene la información mostrada en el listado general de películas.
SerieDetails: Contiene los detalles de una serie.
SeriesResult: Contiene la información mostrada en el listado general de series.
```

## Capas de la aplicación: Vistas

Son las encargadas de mostrar los datos. La información mostrada en estas es proveída por el presenter asociado a la misma. En la estructura del proyecto estas se encuentran bajo el paquete "vistas" separadas por paquetes pertenecientes a cada modulo (Ej: la anidación de paquetes para UpcomingSeriesFragment sería vistas -> series -> upcomingSeries).

Vistas de la aplicación:

```
MainActivity: Es la que maneja el Navigation Drawer general y el contenedor de fragments principal.
DiscoverFragment: Es el fragmento principal mostrado al iniciar la app y que muestra un listado general de peliculas y series. 
MovieDetailsFragment: Muestra los detalles y video de una película específica.
PopularMoviesFragment: Contiene la lista de películas mas populares en modo descendiente y un filtro superior por Generos de películas.
TopMoviesFragment: Contiene la lista de películas con mejor puntuación en modo descendiente y un filtro superior por Generos de películas.
UpcomingMoviesFragment: Contiene la lista de películas futuras en modo descendiente y un filtro superior por Generos de películas.
SerieDetailsFragment: Muestra los detalles y video de una serie específica.
PopularSeriesFragment: Contiene la lista de series mas populares en modo descendiente y un filtro superior por Generos de series.
TopSeriesFragment: Contiene la lista de series con mejor puntuación en modo descendiente y un filtro superior por Generos de series.
UpcomingSeriesFragment: Contiene la lista de series futuras en modo descendiente y un filtro superior por Generos de series.
```

## Capas de la aplicación: Presentadores

Son los encargados de solicitar la data a ser manejada por el usuario y entregarla a la vista a traves de un listener. El presentador es quien decide de donde proveer la data (en este caso de la Api o de la base de datos interna).

Presentadores de la aplicación:

```
DiscoverPresenter: Provee de la lista de 20 primeras peliculas y series de cada categoría para ser mostrados en DiscoverFragment.
MovieDetailsPresenter: Provee la información de una pelicula específica.
PopularMoviesPresenter: Provee la información sobre los Generos de películas y el listado de películas populares en base a los filtros seleccionados.
TopMoviesPresenter: Provee la información sobre los Generos de películas y el listado de películas con mejor puntuación en base a los filtros seleccionados.
UpcomingMoviesPresenter: Provee la información sobre los Generos de películas y el listado de películas futuras en base a los filtros seleccionados.
SerieDetailsPresenter: Provee la información de una serie específica.
PopularSeriesPresenter: Provee la información sobre los Generos de series y el listado de series populares en base a los filtros seleccionados.
TopSeriesPresenter: Provee la información sobre los Generos de series y el listado de series con mejor puntuación en base a los filtros seleccionados.
UpcomingSeriesPresenter: Provee la información sobre los Generos de series y el listado de series futuras en base a los filtros seleccionados.
```

## Capas de la aplicación: Adicionales

Además de las clases anteriormente mencionadas, la aplicación contiene otras clases adicionales que cumplen con funciones especificas como injección de dependencias e interfaces para las consultas Http. Estas son:

```
AppComponent: Define los modulos a ser inyectados utilizando Dagger2 y las vistas en las que pueden ser inyectados.
AppModule: Modulo de Dagger2 que provee del Context de la app.
ApiModule: Modulo de Dagger2 que genera las instancias de Retrofit2, ApiInterface y Realm que seran inyectados en otros componentes o vistas.
ControllerModule: Modulo de Dagger2 que genera las instancias de MoviesController y SeriesController para ser inyectados en otros componentes o vistas.
ApiService: Interfaz de Retrofit2 que define las consultas Http que se realizaran.
MoviesController: Clase que se comunica con el apiService inyectado y que funciona de intermediario entre Retrofit2 y los Presentadores relacionados con películas.
SeriesController: Clase que se comunica con el apiService inyectado y que funciona de intermediario entre Retrofit2 y los Presentadores relacionados con series.
```

## ¿En qué consiste el principio de responsabilidad única? ¿Cuál es su propósito?

El principio de responsabilidad única es el primer principio del acronimo SOLID. Este dicta que cada módulo o clase solo deberia tener responsabilidad sobre una unica parte de la funcionalidad provista por el software, por lo que si esta se ocupa de diferentes cosas debemos separarlo en diferentes clases que se encarguen de esa unica funcionalidad, la cual debe estar encapsulada en su totalidad por la clase de manera que si es necesario realizar un cambio en el software este se realiza en la clase asociada a dicha responsabilidad y sea transparente para las otras clases sin importar si dependen de ella.

Su propósito es ayudar a que el código tenga una alta cohesión y un bajo acoplamiento, con esto se consigue mayor mantenibilidad del software y un código mas limpio, haciéndolo más facil de mantener.

## ¿Qué características tiene, según su opinión, un “buen” código o código limpio?

Debe ser legible y entendible para cualquier programador con un nivel basico de conocimientos sin mucho contexto previo sobre el funcionamiento del mismo, de igual manera los metodos y atributos de clases deberian tener nombres relacionados a su funcion en el software. El reutilizamiento de código también es importante para evitar inconsistencias generadas al realizar mantenimiento a codigos duplicados.