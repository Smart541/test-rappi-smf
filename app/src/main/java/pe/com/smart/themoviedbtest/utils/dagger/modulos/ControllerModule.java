package pe.com.smart.themoviedbtest.utils.dagger.modulos;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import pe.com.smart.themoviedbtest.api.controladores.MoviesController;
import pe.com.smart.themoviedbtest.api.controladores.SeriesController;
import pe.com.smart.themoviedbtest.api.servicios.ApiService;

@Module
public class ControllerModule {

    @Provides
    @Singleton
    MoviesController moviesController(ApiService apiService) {
        return new MoviesController(apiService);
    }

    @Provides
    @Singleton
    SeriesController seriesController(ApiService apiService) {
        return new SeriesController(apiService);
    }

}
