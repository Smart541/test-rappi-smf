package pe.com.smart.themoviedbtest.vistas.series.seriesDetails;

import pe.com.smart.themoviedbtest.modelos.series.SerieDetails;
import pe.com.smart.themoviedbtest.vistas.base.BaseView;

public interface SerieDetailsView extends BaseView {
    void setData(SerieDetails serie);
}
