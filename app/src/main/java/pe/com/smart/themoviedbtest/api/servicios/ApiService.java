package pe.com.smart.themoviedbtest.api.servicios;

import pe.com.smart.themoviedbtest.modelos.genres.GenreListing;
import pe.com.smart.themoviedbtest.modelos.movies.MovieDetails;
import pe.com.smart.themoviedbtest.modelos.movies.MovieListing;
import pe.com.smart.themoviedbtest.modelos.series.SerieDetails;
import pe.com.smart.themoviedbtest.modelos.series.SeriesListing;
import pe.com.smart.themoviedbtest.utils.Constantes;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

public interface ApiService {

    @GET("discover/movie?api_key=" + Constantes.API_KEY)
    Observable<MovieListing> getMovies(@Query("sort_by") String sort_by,
                                       @Query("page") int page,
                                       @Query("primary_release_date.gte") String early,
                                       @Query("primary_release_date.lte") String late,
                                       @Query("vote_count.gte") int votes,
                                       @Query("language") String language);

    @GET("discover/movie?api_key=" + Constantes.API_KEY)
    Observable<MovieListing> getFilteredMovies(@Query("sort_by") String sort_by,
                                               @Query("page") int page,
                                               @Query("primary_release_date.gte") String early,
                                               @Query("primary_release_date.lte") String late,
                                               @Query("vote_count.gte") int votes,
                                               @Query("with_genres") String genresFilter,
                                               @Query("language") String language);

    @GET("discover/tv?api_key=" + Constantes.API_KEY)
    Observable<SeriesListing> getSeries(@Query("sort_by") String sort_by,
                                        @Query("page") int page,
                                        @Query("first_air_date.gte") String early,
                                        @Query("first_air_date.lte") String late,
                                        @Query("vote_count.gte") int votes,
                                        @Query("language") String language);

    @GET("discover/tv?api_key=" + Constantes.API_KEY)
    Observable<SeriesListing> getFilteredSeries(@Query("sort_by") String sort_by,
                                                @Query("page") int page,
                                                @Query("first_air_date.gte") String early,
                                                @Query("first_air_date.lte") String late,
                                                @Query("vote_count.gte") int votes,
                                                @Query("with_genres") String genresFilter,
                                                @Query("language") String language);

    @GET("movie/{movieid}?api_key=" + Constantes.API_KEY + "&append_to_response=videos")
    Observable<MovieDetails> getMovieDetails(@Path("movieid") int movie_id,
                                             @Query("language") String language);

    @GET("tv/{serieid}?api_key=" + Constantes.API_KEY + "&append_to_response=videos")
    Observable<SerieDetails> getSerieDetails(@Path("serieid") int serie_id,
                                             @Query("language") String language);

    @GET("genre/movie/list?api_key=" + Constantes.API_KEY)
    Observable<GenreListing> getMovieGenres(@Query("language") String language);

    @GET("genre/tv/list?api_key=" + Constantes.API_KEY)
    Observable<GenreListing> getSeriesGenres(@Query("language") String language);

}
