package pe.com.smart.themoviedbtest.utils.dagger.componentes;

import javax.inject.Singleton;

import dagger.Component;
import pe.com.smart.themoviedbtest.utils.dagger.modulos.ApiModule;
import pe.com.smart.themoviedbtest.utils.dagger.modulos.AppModule;
import pe.com.smart.themoviedbtest.utils.dagger.modulos.ControllerModule;
import pe.com.smart.themoviedbtest.vistas.discover.DiscoverFragment;
import pe.com.smart.themoviedbtest.vistas.movies.movieDetails.MovieDetailsFragment;
import pe.com.smart.themoviedbtest.vistas.movies.popularMovies.PopularMoviesFragment;
import pe.com.smart.themoviedbtest.vistas.movies.topMovies.TopMoviesFragment;
import pe.com.smart.themoviedbtest.vistas.movies.upcomingMovies.UpcomingMoviesFragment;
import pe.com.smart.themoviedbtest.vistas.series.popularSeries.PopularSeriesFragment;
import pe.com.smart.themoviedbtest.vistas.series.seriesDetails.SerieDetailsFragment;
import pe.com.smart.themoviedbtest.vistas.series.topSeries.TopSeriesFragment;
import pe.com.smart.themoviedbtest.vistas.series.upcomingSeries.UpcomingSeriesFragment;

@Singleton
@Component(
        modules = {
                AppModule.class,
                ApiModule.class,
                ControllerModule.class
        }
)

public interface AppComponent {

    void inject(DiscoverFragment discoverFragment);

    void inject(PopularMoviesFragment popularMoviesFragment);

    void inject(MovieDetailsFragment movieDetailsFragment);

    void inject(PopularSeriesFragment popularSeriesFragment);

    void inject(SerieDetailsFragment serieDetailsFragment);

    void inject(TopMoviesFragment topMoviesFragment);

    void inject(UpcomingMoviesFragment upcomingMoviesFragment);

    void inject(TopSeriesFragment topSeriesFragment);

    void inject(UpcomingSeriesFragment upcomingSeriesFragment);
}
