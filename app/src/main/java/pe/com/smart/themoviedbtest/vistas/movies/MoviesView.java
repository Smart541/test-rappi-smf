package pe.com.smart.themoviedbtest.vistas.movies;

import pe.com.smart.themoviedbtest.modelos.genres.GenreListing;
import pe.com.smart.themoviedbtest.modelos.movies.MovieListing;
import pe.com.smart.themoviedbtest.vistas.base.BaseView;

public interface MoviesView extends BaseView {
    void updateAdapter(MovieListing movieListing);

    void setAdapter(MovieListing list);

    void setFilters(GenreListing genreListing);
}
