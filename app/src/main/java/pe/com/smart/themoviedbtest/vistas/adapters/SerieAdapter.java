package pe.com.smart.themoviedbtest.vistas.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;
import pe.com.smart.themoviedbtest.R;
import pe.com.smart.themoviedbtest.modelos.series.SeriesResult;
import pe.com.smart.themoviedbtest.utils.Constantes;
import pe.com.smart.themoviedbtest.utils.ScrollRefreshInterface;
import pe.com.smart.themoviedbtest.vistas.main.MainListener;

public class SerieAdapter extends RecyclerView.Adapter<ItemViewHolder> {
    private static final String TAG = SerieAdapter.class.getSimpleName();

    private List<SeriesResult> series;
    private ScrollRefreshInterface anInterface;
    private MainListener parentActivity;

    public SerieAdapter(List<SeriesResult> series, MainListener parentActivity, ScrollRefreshInterface anInterface) {
        this.series = series;
        this.anInterface = anInterface;
        this.parentActivity = parentActivity;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recicler_item, parent, false);
        return new ItemViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {

        holder.item_number.setText(String.valueOf(position + 1));
        holder.item_rating.setText(String.valueOf(series.get(position).getVoteAverage()));
        if (series.get(position).getPosterPath() != null)
            Picasso.get().load(Constantes.URL_IMAGES + Constantes.POSTER_SIZE + series.get(position).getPosterPath())
                    .placeholder(R.drawable.nothumbnail).into(holder.item_image);

        if ((position == series.size() - 1) && (anInterface != null)) {
            anInterface.loadNextPage();
        }

        holder.item_bg.setOnClickListener(v -> parentActivity.showDetail(Constantes.SERIE_DETAIL, series.get(holder.getAdapterPosition()).getId()));
    }

    @Override
    public int getItemCount() {
        return series.size();
    }
}
