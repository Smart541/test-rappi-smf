package pe.com.smart.themoviedbtest.vistas.discover;

import pe.com.smart.themoviedbtest.modelos.movies.MovieListing;
import pe.com.smart.themoviedbtest.modelos.series.SeriesListing;
import pe.com.smart.themoviedbtest.vistas.base.BaseView;

public interface DiscoverView extends BaseView {
    void setPopularMoviesAdapter(MovieListing movies);

    void setTopRatedMoviesAdapter(MovieListing movies);

    void setUpcomingMoviesAdapter(MovieListing movies);

    void setPopularSeriesAdapter(SeriesListing series);

    void setTopRatedSeriesAdapter(SeriesListing series);

    void setUpcomingSeriesAdapter(SeriesListing series);
}
