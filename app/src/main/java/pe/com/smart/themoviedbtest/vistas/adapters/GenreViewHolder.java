package pe.com.smart.themoviedbtest.vistas.adapters;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import pe.com.smart.themoviedbtest.R;

public class GenreViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.img_state)
    ImageView imgState;

    @BindView(R.id.tv_genre_name)
    TextView tvGenreName;

    @BindView(R.id.item_bg)
    LinearLayout itemBg;

    public GenreViewHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
    }
}
