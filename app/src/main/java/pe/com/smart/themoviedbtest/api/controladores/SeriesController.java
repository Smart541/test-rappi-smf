package pe.com.smart.themoviedbtest.api.controladores;

import pe.com.smart.themoviedbtest.api.servicios.ApiService;
import pe.com.smart.themoviedbtest.modelos.genres.GenreListing;
import pe.com.smart.themoviedbtest.modelos.series.SerieDetails;
import pe.com.smart.themoviedbtest.modelos.series.SeriesListing;
import pe.com.smart.themoviedbtest.utils.Utils;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class SeriesController {

    private ApiService apiService;

    public SeriesController(ApiService apiService) {
        this.apiService = apiService;
    }

    public Observable<SeriesListing> getSeries(String sort_by, int page, String early, String late, int votes) {
        return apiService.getSeries(sort_by, page, early, late, votes, Utils.getLocalLang())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<SeriesListing> getFilteredSeries(String sort_by, int page, String early, String late, int votes, String genresFilter) {
        return apiService.getFilteredSeries(sort_by, page, early, late, votes, genresFilter, Utils.getLocalLang())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<SerieDetails> getSerieDetails(int serieId) {
        return apiService.getSerieDetails(serieId, Utils.getLocalLang())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<GenreListing> getSeriesGenres() {
        return apiService.getSeriesGenres(Utils.getLocalLang())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
