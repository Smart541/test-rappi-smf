package pe.com.smart.themoviedbtest.vistas.main;

public interface MainListener {

    void setTitle(String title);

    void setCheckedItem(int selectedNav);

    void swapDrawerMode(int drawerMode);

    void showDetail(int itemType, int itemId);
}
