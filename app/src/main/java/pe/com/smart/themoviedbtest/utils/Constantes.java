package pe.com.smart.themoviedbtest.utils;

public interface Constantes {

    String API_KEY = "eb17e443ae62c9771628ab295be96a17";
    String YOUTUBE_API_KEY = "AIzaSyDOmtw5ZlsyJUg8AhDtuR-4YCwbXWiV4-0";
    String URL_IMAGES = "https://image.tmdb.org/t/p/";
    String POSTER_SIZE = "w500";
    String BACKDROP_SIZE = "w1280";

    String SORTING_POPULARITY = "popularity.desc";
    String SORTING_TOPRATED = "vote_average.desc";
    String SORTING_UPCOMING = "release_date.asc";
    String SORTING_FIRSTAIRDATE = "first_air_date.desc";

    int MINIMUM_VOTES = 500;

    int DRAWER_ENABLED = 1;
    int DRAWER_DISABLED = 0;

    int MOVIE_DETAIL = 0;
    int SERIE_DETAIL = 1;

}
