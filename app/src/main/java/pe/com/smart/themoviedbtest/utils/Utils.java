package pe.com.smart.themoviedbtest.utils;

import java.util.Locale;

public class Utils {
    public static String getLocalLang() {
        return Locale.getDefault().getLanguage();
    }
}
