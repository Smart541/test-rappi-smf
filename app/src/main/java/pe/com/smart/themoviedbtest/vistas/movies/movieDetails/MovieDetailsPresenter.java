package pe.com.smart.themoviedbtest.vistas.movies.movieDetails;

import io.realm.Realm;
import io.realm.RealmResults;
import pe.com.smart.themoviedbtest.api.controladores.MoviesController;
import pe.com.smart.themoviedbtest.modelos.movies.MovieDetails;

public class MovieDetailsPresenter {

    private static final String TAG = MovieDetailsPresenter.class.getSimpleName();

    private MoviesController moviesController;
    private Realm realm;
    private MovieDetailsView movieDetailsView;
    private int id = -1;

    public MovieDetailsPresenter(MoviesController moviesController, Realm realm, MovieDetailsView movieDetailsView) {
        this.moviesController = moviesController;
        this.realm = realm;
        this.movieDetailsView = movieDetailsView;
    }

    public void getMovieDetails(int movieId) {
        this.id = movieId;
        moviesController.getMovieDetails(movieId)
                .subscribe(this::onMovieDetailSuccess, this::onMovieDetailFailure);
    }

    private void onMovieDetailSuccess(MovieDetails movie) {
        realm.beginTransaction();
        realm.insertOrUpdate(movie);
        realm.commitTransaction();
        movieDetailsView.setData(movie);
    }

    private void onMovieDetailFailure(Throwable throwable) {
        RealmResults<MovieDetails> realmResults = realm.where(MovieDetails.class)
                .equalTo("id", this.id)
                .findAll();
        if (realmResults.size() > 0) {
            movieDetailsView.setData(realmResults.get(0));
        } else {
            movieDetailsView.onFailure("");
        }
    }

}
