package pe.com.smart.themoviedbtest.utils;

public interface ScrollRefreshInterface {
    void loadNextPage();
}
