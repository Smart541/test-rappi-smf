package pe.com.smart.themoviedbtest.vistas.main;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import butterknife.BindView;
import pe.com.smart.themoviedbtest.R;
import pe.com.smart.themoviedbtest.utils.Constantes;
import pe.com.smart.themoviedbtest.vistas.base.BaseActivity;
import pe.com.smart.themoviedbtest.vistas.discover.DiscoverFragment;
import pe.com.smart.themoviedbtest.vistas.movies.movieDetails.MovieDetailsFragment;
import pe.com.smart.themoviedbtest.vistas.movies.popularMovies.PopularMoviesFragment;
import pe.com.smart.themoviedbtest.vistas.movies.topMovies.TopMoviesFragment;
import pe.com.smart.themoviedbtest.vistas.movies.upcomingMovies.UpcomingMoviesFragment;
import pe.com.smart.themoviedbtest.vistas.series.popularSeries.PopularSeriesFragment;
import pe.com.smart.themoviedbtest.vistas.series.seriesDetails.SerieDetailsFragment;
import pe.com.smart.themoviedbtest.vistas.series.topSeries.TopSeriesFragment;
import pe.com.smart.themoviedbtest.vistas.series.upcomingSeries.UpcomingSeriesFragment;

import android.os.Bundle;
import android.view.MenuItem;

import com.google.android.material.navigation.NavigationView;

public class MainActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener, MainListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.navview)
    NavigationView navView;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;

    private ActionBarDrawerToggle toggle;

    @Override
    public int getLayout() {
        return R.layout.activity_main_drawer;
    }

    @Override
    public void onCreateView(Bundle savedInstanceState) {
        setSupportActionBar(toolbar);
        setDrawerToggle();
        pushFragment(DiscoverFragment.newInstance(this), false, true);
    }

    public void setDrawerToggle() {
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        navView.setNavigationItemSelectedListener(this);
        toggle.syncState();
    }

    @Override
    public void swapDrawerMode(int drawerMode) {

        if (getSupportActionBar() != null) {
            switch (drawerMode) {

                case Constantes.DRAWER_DISABLED: {
                    toggle.setDrawerIndicatorEnabled(false);
                    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                    drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                    toggle.setToolbarNavigationClickListener(v -> {
                        onBackPressed();
                        swapDrawerMode(Constantes.DRAWER_ENABLED);
                    });
                    break;
                }

                case Constantes.DRAWER_ENABLED: {
                    getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                    toggle.setDrawerIndicatorEnabled(true);
                    drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
                    toggle.syncState();
                    break;
                }
            }
        }
    }

    @Override
    public void showDetail(int itemType, int itemId) {
        switch (itemType) {
            case Constantes.MOVIE_DETAIL: {
                pushFragment(MovieDetailsFragment.newInstance(itemId, this), true, false);
                break;
            }
            case Constantes.SERIE_DETAIL: {
                pushFragment(SerieDetailsFragment.newInstance(itemId, this), true, false);
                break;
            }
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        switch (id) {

            case R.id.nav_discover: {
                pushFragment(DiscoverFragment.newInstance(this), false, true);
                break;
            }

            case R.id.nav_movies_popular: {
                pushFragment(PopularMoviesFragment.newInstance(this), true, false);
                break;
            }

            case R.id.nav_movies_top: {
                pushFragment(TopMoviesFragment.newInstance(this), true, false);
                break;
            }

            case R.id.nav_movies_upcoming: {
                pushFragment(UpcomingMoviesFragment.newInstance(this), true, false);
                break;
            }

            case R.id.nav_tv_popular: {
                pushFragment(PopularSeriesFragment.newInstance(this), true, false);
                break;
            }

            case R.id.nav_tv_top: {
                pushFragment(TopSeriesFragment.newInstance(this), true, false);
                break;
            }

            case R.id.nav_tv_upcoming: {
                pushFragment(UpcomingSeriesFragment.newInstance(this), true, false);
                break;
            }
        }
        closeDrawer();
        return true;
    }

    @Override
    public void setTitle(String title) {
        getSupportActionBar().setTitle(title);
    }

    @Override
    public void setCheckedItem(int selectedNav) {
        navView.setCheckedItem(selectedNav);
    }

    public void closeDrawer() {
        if (this.drawer != null && isOpenDrawerLayout()) {
            this.drawer.closeDrawer(GravityCompat.START);
        }
    }

    public boolean isOpenDrawerLayout() {
        return drawer.isDrawerOpen(GravityCompat.START);
    }

}
