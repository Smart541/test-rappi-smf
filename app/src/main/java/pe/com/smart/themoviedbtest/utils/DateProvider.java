package pe.com.smart.themoviedbtest.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class DateProvider {

    public static String GetCurrentDate() {

        Calendar c = Calendar.getInstance();

        SimpleDateFormat df = new SimpleDateFormat("yyyy-dd-MM");

        return df.format(c.getTime());
    }

    public static String GetFutureDate() {
        Calendar c = Calendar.getInstance();

        c.add(Calendar.YEAR, 1);
        SimpleDateFormat df = new SimpleDateFormat("yyyy-dd-MM");

        return df.format(c.getTime());
    }
}
