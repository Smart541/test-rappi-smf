package pe.com.smart.themoviedbtest.vistas.base;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;

import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import butterknife.BindView;
import butterknife.ButterKnife;
import pe.com.smart.themoviedbtest.R;
import pe.com.smart.themoviedbtest.utils.dagger.App;
import pe.com.smart.themoviedbtest.utils.dagger.componentes.AppComponent;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public abstract class BaseActivity extends AppCompatActivity {

    @BindView(R.id.loading_bar)
    FrameLayout loading;

    private final String TAG = BaseActivity.class.getSimpleName();
    protected FragmentManager fragMan;

    abstract public int getLayout();

    abstract public void onCreateView(Bundle savedInstanceState);

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayout());
        ButterKnife.bind(this);
        fragMan = getSupportFragmentManager();
        onCreateView(savedInstanceState);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    public AppComponent getAppComponent() {
        return ((App) getApplication()).getAppComponent();
    }

    public void showSnackbar(String message) {
        Snackbar.make(loading, message, Snackbar.LENGTH_LONG)
                .show();
    }

    public void showSnackbar(String message, String buttonMessage, View.OnClickListener listener) {
        Snackbar.make(loading, message, Snackbar.LENGTH_LONG).setAction(buttonMessage, listener)
                .show();
    }

    public void toggleLoading(boolean toggle) {
        if (toggle)
            loading.setVisibility(View.VISIBLE);
        else
            loading.setVisibility(View.GONE);
    }

    public void pushFragment(Fragment fragment, boolean addBackStack, boolean isMain) {
        FragmentTransaction transaction = fragMan.beginTransaction();
        String tag = fragment.getClass().getSimpleName();

        if (isMain) {
            while (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                getSupportFragmentManager().popBackStackImmediate();
            }
        }
        if (addBackStack) {
            transaction.addToBackStack(tag);
        }
        transaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out,
                android.R.anim.fade_in, android.R.anim.fade_out);
        transaction.replace(R.id.fragment_container, fragment, tag);
        try {
            transaction.commit();
        } catch (Exception e) {
            Log.d(TAG, "pushFragment error: " + e.getMessage());
        }
    }

    @Override
    public void onBackPressed() {
        if (!fragMan.popBackStackImmediate()) {
            super.onBackPressed();
        }
    }

}
