package pe.com.smart.themoviedbtest.api.controladores;


import pe.com.smart.themoviedbtest.api.servicios.ApiService;
import pe.com.smart.themoviedbtest.modelos.genres.GenreListing;
import pe.com.smart.themoviedbtest.modelos.movies.MovieDetails;
import pe.com.smart.themoviedbtest.modelos.movies.MovieListing;
import pe.com.smart.themoviedbtest.utils.Utils;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class MoviesController {

    private ApiService apiService;

    public MoviesController(ApiService apiService) {
        this.apiService = apiService;
    }

    public Observable<MovieListing> getMovies(String sort_by, int page, String early, String late, int votes) {
        return apiService.getMovies(sort_by, page, early, late, votes, Utils.getLocalLang())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<MovieListing> getFilteredMovies(String sort_by, int page, String early, String late, int votes, String genresFilter) {
        return apiService.getFilteredMovies(sort_by, page, early, late, votes, genresFilter, Utils.getLocalLang())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<MovieDetails> getMovieDetails(int movieId) {
        return apiService.getMovieDetails(movieId, Utils.getLocalLang())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<GenreListing> getMovieGenres() {
        return apiService.getMovieGenres(Utils.getLocalLang())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
