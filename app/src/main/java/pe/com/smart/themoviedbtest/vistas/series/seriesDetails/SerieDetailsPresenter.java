package pe.com.smart.themoviedbtest.vistas.series.seriesDetails;

import io.realm.Realm;
import io.realm.RealmResults;
import pe.com.smart.themoviedbtest.api.controladores.SeriesController;
import pe.com.smart.themoviedbtest.modelos.series.SerieDetails;

public class SerieDetailsPresenter {

    private static final String TAG = SerieDetailsPresenter.class.getSimpleName();

    private SeriesController seriesController;
    private Realm realm;
    private SerieDetailsView serieDetailsView;
    private int id = -1;

    public SerieDetailsPresenter(SeriesController seriesController, Realm realm, SerieDetailsView serieDetailsView) {
        this.seriesController = seriesController;
        this.realm = realm;
        this.serieDetailsView = serieDetailsView;
    }

    public void getSerieDetails(int movieId) {
        this.id = movieId;
        seriesController.getSerieDetails(movieId)
                .subscribe(this::onSerieDetailSuccess, this::onSerieDetailFailure);
    }

    private void onSerieDetailSuccess(SerieDetails serie) {
        realm.beginTransaction();
        realm.insertOrUpdate(serie);
        realm.commitTransaction();
        serieDetailsView.setData(serie);
    }

    private void onSerieDetailFailure(Throwable throwable) {
        RealmResults<SerieDetails> realmResults = realm.where(SerieDetails.class)
                .equalTo("id", this.id)
                .findAll();
        if (realmResults.size() > 0) {
            serieDetailsView.setData(realmResults.get(0));
        } else {
            serieDetailsView.onFailure("");
        }
    }

}
