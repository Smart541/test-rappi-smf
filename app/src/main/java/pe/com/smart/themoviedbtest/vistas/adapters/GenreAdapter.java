package pe.com.smart.themoviedbtest.vistas.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import pe.com.smart.themoviedbtest.R;
import pe.com.smart.themoviedbtest.modelos.genres.Genre;

public class GenreAdapter extends RecyclerView.Adapter<GenreViewHolder> {

    private static final String TAG = GenreAdapter.class.getSimpleName();

    private List<Genre> genres;
    private FiltersInterface parent;
    private Context context;

    public GenreAdapter(List<Genre> genres, FiltersInterface parent) {
        this.genres = genres;
        this.parent = parent;
    }

    @Override
    public GenreViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.filter_item, parent, false);
        this.context = v.getContext();
        return new GenreViewHolder(v);
    }

    @Override
    public void onBindViewHolder(GenreViewHolder holder, int position) {

        Genre item = genres.get(position);
        holder.tvGenreName.setText(item.getName());

        if (item.isSelected()) {
            holder.imgState.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.baseline_check_circle_black_18));
            holder.itemBg.setBackground(ContextCompat.getDrawable(context, R.drawable.selected_filter_chip));
        } else {
            holder.imgState.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.baseline_add_circle_black_18));
            holder.itemBg.setBackground(ContextCompat.getDrawable(context, R.drawable.removed_filter_chip));
        }

        holder.itemBg.setOnClickListener(v -> {
            item.setSelected(!item.isSelected());
            notifyDataSetChanged();
            parent.reloadData();
        });
    }

    @Override
    public int getItemCount() {
        return genres.size();
    }

    public String getSelected() {
        String selectedGenres = "";
        for (Genre item : genres) {
            if (item.isSelected()) {
                selectedGenres = selectedGenres.concat(item.getId() + "|");
            }
        }
        return selectedGenres;
    }
}
