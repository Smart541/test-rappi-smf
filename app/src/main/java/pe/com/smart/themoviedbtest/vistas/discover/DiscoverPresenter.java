package pe.com.smart.themoviedbtest.vistas.discover;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;
import pe.com.smart.themoviedbtest.api.controladores.MoviesController;
import pe.com.smart.themoviedbtest.api.controladores.SeriesController;
import pe.com.smart.themoviedbtest.modelos.movies.MovieListing;
import pe.com.smart.themoviedbtest.modelos.movies.MovieResult;
import pe.com.smart.themoviedbtest.modelos.series.SeriesListing;
import pe.com.smart.themoviedbtest.modelos.series.SeriesResult;
import pe.com.smart.themoviedbtest.utils.Constantes;

public class DiscoverPresenter {

    private static final String TAG = DiscoverPresenter.class.getSimpleName();
    private MoviesController moviesController;
    private SeriesController seriesController;
    private Realm realm;
    private DiscoverView discoverView;

    public DiscoverPresenter(MoviesController moviesController, SeriesController seriesController, Realm realm, DiscoverView discoverView) {
        this.moviesController = moviesController;
        this.seriesController = seriesController;
        this.realm = realm;
        this.discoverView = discoverView;
    }

    void getDiscoverData() {
        moviesController.getMovies(Constantes.SORTING_POPULARITY, 1, null, null, 0)
                .subscribe(this::onPopularMoviesSuccess, this::onPopularMoviesFailure);
        moviesController.getMovies(Constantes.SORTING_TOPRATED, 1, null, null, Constantes.MINIMUM_VOTES)
                .subscribe(this::onTopRatedMoviesSuccess, this::onTopRatedMoviesFailure);
        moviesController.getMovies(Constantes.SORTING_UPCOMING, 1, null, null, 0)
                .subscribe(this::onUpcomingMoviesSuccess, this::onUpcomingMoviesFailure);

        seriesController.getSeries(Constantes.SORTING_POPULARITY, 1, null, null, 0)
                .subscribe(this::onPopularSeriesSuccess, this::onPopularSeriesFailure);
        seriesController.getSeries(Constantes.SORTING_TOPRATED, 1, null, null, Constantes.MINIMUM_VOTES)
                .subscribe(this::onTopRatedSeriesSuccess, this::onTopRatedSeriesFailure);
        seriesController.getSeries(Constantes.SORTING_FIRSTAIRDATE, 1, null, null, 0)
                .subscribe(this::onUpcomingSeriesSuccess, this::onUpcomingSeriesFailure);

        //en las upcoming se quiso filtrar por fecha para que solo mostrara las del año en curso (2019-2020 por ejemplo) pero el filtro por fechas de la api esta fallando
    }

    private void onPopularMoviesSuccess(MovieListing movies) {
        realm.beginTransaction();
        realm.insertOrUpdate(movies.getResults());
        realm.commitTransaction();
        discoverView.setPopularMoviesAdapter(movies);
    }

    private void onTopRatedMoviesSuccess(MovieListing movies) {
        realm.beginTransaction();
        realm.insertOrUpdate(movies.getResults());
        realm.commitTransaction();
        discoverView.setTopRatedMoviesAdapter(movies);
    }

    private void onUpcomingMoviesSuccess(MovieListing movies) {
        realm.beginTransaction();
        realm.insertOrUpdate(movies.getResults());
        realm.commitTransaction();
        discoverView.setUpcomingMoviesAdapter(movies);
    }

    private void onPopularSeriesSuccess(SeriesListing series) {
        realm.beginTransaction();
        realm.insertOrUpdate(series.getResults());
        realm.commitTransaction();
        discoverView.setPopularSeriesAdapter(series);
    }

    private void onTopRatedSeriesSuccess(SeriesListing series) {
        realm.beginTransaction();
        realm.insertOrUpdate(series.getResults());
        realm.commitTransaction();
        discoverView.setTopRatedSeriesAdapter(series);
    }

    private void onUpcomingSeriesSuccess(SeriesListing series) {
        realm.beginTransaction();
        realm.insertOrUpdate(series.getResults());
        realm.commitTransaction();
        discoverView.setUpcomingSeriesAdapter(series);
    }

    private void onPopularMoviesFailure(Throwable throwable) {
        RealmResults<MovieResult> realmResults = realm.where(MovieResult.class)
                .sort("popularity", Sort.DESCENDING).limit(20).findAll();

        if (realmResults.size() > 0) {
            List<MovieResult> results = realm.copyFromRealm(realmResults);
            MovieListing list = new MovieListing();
            list.setResults(results);
            list.setPage(1);
            list.setTotalPages(1);
            list.setTotalResults(results.size());
            discoverView.setPopularMoviesAdapter(list);
        } else {
            discoverView.onFailure("Failure");
        }
    }

    private void onTopRatedMoviesFailure(Throwable throwable) {
        RealmResults<MovieResult> realmResults = realm.where(MovieResult.class)
                .greaterThanOrEqualTo("voteCount", Constantes.MINIMUM_VOTES)
                .sort("voteAverage", Sort.DESCENDING).limit(20).findAll();

        if (realmResults.size() > 0) {
            List<MovieResult> results = realm.copyFromRealm(realmResults);
            MovieListing list = new MovieListing();
            list.setResults(results);
            list.setPage(1);
            list.setTotalPages(1);
            list.setTotalResults(results.size());
            discoverView.setTopRatedMoviesAdapter(list);
        } else {
            discoverView.onFailure("Failure");
        }
    }

    private void onUpcomingMoviesFailure(Throwable throwable) {
        RealmResults<MovieResult> realmResults = realm.where(MovieResult.class)
                .sort("releaseDate", Sort.ASCENDING).limit(20).findAll();

        if (realmResults.size() > 0) {
            List<MovieResult> results = realm.copyFromRealm(realmResults);
            MovieListing list = new MovieListing();
            list.setResults(results);
            list.setPage(1);
            list.setTotalPages(1);
            list.setTotalResults(results.size());
            discoverView.setUpcomingMoviesAdapter(list);
        } else {
            discoverView.onFailure("Failure");
        }
    }

    private void onPopularSeriesFailure(Throwable throwable) {
        RealmResults<SeriesResult> realmResults = realm.where(SeriesResult.class)
                .sort("popularity", Sort.DESCENDING).limit(20).findAll();

        if (realmResults.size() > 0) {
            List<SeriesResult> results = realm.copyFromRealm(realmResults);
            SeriesListing list = new SeriesListing();
            list.setResults(results);
            list.setPage(1);
            list.setTotalPages(1);
            list.setTotalResults(results.size());
            discoverView.setPopularSeriesAdapter(list);
        } else {
            discoverView.onFailure("Failure");
        }
    }

    private void onTopRatedSeriesFailure(Throwable throwable) {
        RealmResults<SeriesResult> realmResults = realm.where(SeriesResult.class)
                .greaterThanOrEqualTo("voteCount", Constantes.MINIMUM_VOTES)
                .sort("voteAverage", Sort.DESCENDING).limit(20).findAll();

        if (realmResults.size() > 0) {
            List<SeriesResult> results = realm.copyFromRealm(realmResults);
            SeriesListing list = new SeriesListing();
            list.setResults(results);
            list.setPage(1);
            list.setTotalPages(1);
            list.setTotalResults(results.size());
            discoverView.setTopRatedSeriesAdapter(list);
        } else {
            discoverView.onFailure("Failure");
        }
    }

    private void onUpcomingSeriesFailure(Throwable throwable) {
        RealmResults<SeriesResult> realmResults = realm.where(SeriesResult.class)
                .sort("firstAirDate", Sort.ASCENDING).limit(20).findAll();

        if (realmResults.size() > 0) {
            List<SeriesResult> results = realm.copyFromRealm(realmResults);
            SeriesListing list = new SeriesListing();
            list.setResults(results);
            list.setPage(1);
            list.setTotalPages(1);
            list.setTotalResults(results.size());
            discoverView.setUpcomingSeriesAdapter(list);
        } else {
            discoverView.onFailure("Failure");
        }
    }

}
