package pe.com.smart.themoviedbtest.vistas.series;

import pe.com.smart.themoviedbtest.modelos.genres.GenreListing;
import pe.com.smart.themoviedbtest.modelos.series.SeriesListing;
import pe.com.smart.themoviedbtest.vistas.base.BaseView;

public interface SeriesView extends BaseView {
    void updateAdapter(SeriesListing seriesListing);

    void setAdapter(SeriesListing list);

    void setFilters(GenreListing genreListing);
}
