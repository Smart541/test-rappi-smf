package pe.com.smart.themoviedbtest.utils.dagger.modulos;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.realm.Realm;
import pe.com.smart.themoviedbtest.BuildConfig;
import pe.com.smart.themoviedbtest.api.servicios.ApiService;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class ApiModule {

    @Provides
    @Singleton
    public Retrofit retrofit() {

        return new Retrofit.Builder()
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BuildConfig.API_URL)
                .build();
    }

    @Provides
    @Singleton
    public ApiService apiInterface(Retrofit retrofit) {
        return retrofit.create(ApiService.class);
    }

    @Provides
    @Singleton
    public Realm realm() {
        return Realm.getDefaultInstance();
    }

}
