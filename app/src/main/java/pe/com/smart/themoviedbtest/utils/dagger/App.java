package pe.com.smart.themoviedbtest.utils.dagger;

import android.app.Application;

import io.realm.Realm;
import pe.com.smart.themoviedbtest.R;
import pe.com.smart.themoviedbtest.utils.dagger.componentes.AppComponent;
import pe.com.smart.themoviedbtest.utils.dagger.componentes.DaggerAppComponent;
import pe.com.smart.themoviedbtest.utils.dagger.modulos.AppModule;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class App extends Application {

    AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/sourcesanspro_regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        Realm.init(this);
        initDagger();
    }

    private void initDagger() {
        appComponent = DaggerAppComponent.builder().appModule(new AppModule(this)).build();
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }

}