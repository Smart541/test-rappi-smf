package pe.com.smart.themoviedbtest.vistas.series.popularSeries;


import android.os.Bundle;

import androidx.annotation.Nullable;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import io.realm.Realm;
import pe.com.smart.themoviedbtest.R;
import pe.com.smart.themoviedbtest.api.controladores.SeriesController;
import pe.com.smart.themoviedbtest.modelos.genres.GenreListing;
import pe.com.smart.themoviedbtest.modelos.series.SeriesListing;
import pe.com.smart.themoviedbtest.modelos.series.SeriesResult;
import pe.com.smart.themoviedbtest.utils.Constantes;
import pe.com.smart.themoviedbtest.utils.ScrollRefreshInterface;
import pe.com.smart.themoviedbtest.vistas.adapters.FiltersInterface;
import pe.com.smart.themoviedbtest.vistas.adapters.GenreAdapter;
import pe.com.smart.themoviedbtest.vistas.adapters.SerieAdapter;
import pe.com.smart.themoviedbtest.vistas.base.BaseFragment;
import pe.com.smart.themoviedbtest.vistas.main.MainListener;
import pe.com.smart.themoviedbtest.vistas.series.SeriesView;

public class PopularSeriesFragment extends BaseFragment implements ScrollRefreshInterface, SeriesView, FiltersInterface {

    @Inject
    SeriesController seriesController;
    @Inject
    Realm realm;

    @BindView(R.id.rv_filters)
    RecyclerView rvFilters;
    @BindView(R.id.rv_list)
    RecyclerView rvPopularSeries;
    @BindView(R.id.background)
    ImageView background;

    private MainListener parentActivity;
    private PopularSeriesPresenter presenter;

    private SerieAdapter mAdapter;
    private GenreAdapter filtersAdapter;
    private List<SeriesResult> movies;
    private int totalPages, currentPage;
    private boolean gettingMovies;
    private String backgroundPath;

    public PopularSeriesFragment() {
        // Required empty public constructor
    }

    public static PopularSeriesFragment newInstance(MainListener parent) {
        PopularSeriesFragment fragment = new PopularSeriesFragment();
        fragment.parentActivity = parent;
        return fragment;
    }

    @Override
    public int getLayoutView() {
        return R.layout.fragment_list;
    }

    @Override
    public void onViewReady(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState, View view) {
        initializeView();
        totalPages = -1;
        currentPage = 1;
        presenter.getSeriesGenres();
    }

    private void initializeView() {
        getApplicationComponent().inject(this);
        parentActivity.setTitle(getString(R.string.title_pop_serie));
        parentActivity.setCheckedItem(R.id.nav_tv_popular);
        parentActivity.swapDrawerMode(Constantes.DRAWER_ENABLED);
        presenter = new PopularSeriesPresenter(seriesController, realm, this);
        rvPopularSeries.setLayoutManager(new GridLayoutManager(getContext(), 2));
        rvFilters.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        movies = new ArrayList<>();
        mAdapter = new SerieAdapter(movies, parentActivity, this);
        rvPopularSeries.setAdapter(mAdapter);
    }

    @Override
    public void setFilters(GenreListing genreListing) {
        this.filtersAdapter = new GenreAdapter(genreListing.getGenres(), this);
        rvFilters.setAdapter(this.filtersAdapter);
        this.reloadData();
    }

    @Override
    public void reloadData() {
        movies.clear();
        presenter.getSeries(currentPage, this.filtersAdapter.getSelected());
        gettingMovies = true;
    }

    @Override
    public void updateAdapter(SeriesListing seriesListing) {
        if (totalPages == -1) {
            totalPages = seriesListing.getTotalPages();
        }
        movies.addAll(seriesListing.getResults());
        mAdapter.notifyDataSetChanged();
        if (backgroundPath == null && seriesListing.getResults().get(0).getBackdropPath() != null) {
            backgroundPath = seriesListing.getResults().get(0).getBackdropPath();
            Picasso.get().load(Constantes.URL_IMAGES + Constantes.BACKDROP_SIZE + backgroundPath)
                    .placeholder(R.drawable.nobackdrop).fit().centerCrop().into(background);
        }
        gettingMovies = false;
    }

    @Override
    public void setAdapter(SeriesListing seriesListing) {
        movies.addAll(seriesListing.getResults());
        mAdapter.notifyDataSetChanged();
        if (backgroundPath == null && seriesListing.getResults().get(0).getBackdropPath() != null) {
            backgroundPath = seriesListing.getResults().get(0).getBackdropPath();
            Picasso.get().load(Constantes.URL_IMAGES + Constantes.BACKDROP_SIZE + backgroundPath)
                    .placeholder(R.drawable.nobackdrop).fit().centerCrop().into(background);
        }
    }

    @Override
    public void loadNextPage() {
        if (currentPage < totalPages && !gettingMovies) {
            currentPage++;
            presenter.getSeries(currentPage, this.filtersAdapter.getSelected());
            gettingMovies = true;
            activity.showSnackbar(getString(R.string.loading_series));
        }
    }

    @Override
    public void onFailure(String message) {
        activity.showSnackbar(message);
    }

}
