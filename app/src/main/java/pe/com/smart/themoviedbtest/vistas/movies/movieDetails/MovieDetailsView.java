package pe.com.smart.themoviedbtest.vistas.movies.movieDetails;

import pe.com.smart.themoviedbtest.modelos.movies.MovieDetails;
import pe.com.smart.themoviedbtest.vistas.base.BaseView;

public interface MovieDetailsView extends BaseView {
    void setData(MovieDetails movie);
}
