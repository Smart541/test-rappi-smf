package pe.com.smart.themoviedbtest.vistas.discover;

import android.os.Bundle;

import androidx.annotation.Nullable;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import javax.inject.Inject;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.OnClick;
import io.realm.Realm;
import pe.com.smart.themoviedbtest.R;
import pe.com.smart.themoviedbtest.api.controladores.MoviesController;
import pe.com.smart.themoviedbtest.api.controladores.SeriesController;
import pe.com.smart.themoviedbtest.modelos.movies.MovieListing;
import pe.com.smart.themoviedbtest.modelos.series.SeriesListing;
import pe.com.smart.themoviedbtest.utils.Constantes;
import pe.com.smart.themoviedbtest.vistas.adapters.MovieAdapter;
import pe.com.smart.themoviedbtest.vistas.adapters.SerieAdapter;
import pe.com.smart.themoviedbtest.vistas.base.BaseFragment;
import pe.com.smart.themoviedbtest.vistas.main.MainListener;
import pe.com.smart.themoviedbtest.vistas.movies.popularMovies.PopularMoviesFragment;
import pe.com.smart.themoviedbtest.vistas.movies.topMovies.TopMoviesFragment;
import pe.com.smart.themoviedbtest.vistas.movies.upcomingMovies.UpcomingMoviesFragment;
import pe.com.smart.themoviedbtest.vistas.series.popularSeries.PopularSeriesFragment;
import pe.com.smart.themoviedbtest.vistas.series.topSeries.TopSeriesFragment;
import pe.com.smart.themoviedbtest.vistas.series.upcomingSeries.UpcomingSeriesFragment;

public class DiscoverFragment extends BaseFragment implements DiscoverView {

    @Inject
    MoviesController moviesController;
    @Inject
    SeriesController seriesController;
    @Inject
    Realm realm;

    @BindView(R.id.cv_popular_movies)
    CardView cvPopularMovies;
    @BindView(R.id.rv_popular_movies)
    RecyclerView rvPopularMovies;

    @BindView(R.id.cv_top_rated_movies)
    CardView cvTopRatedMovies;
    @BindView(R.id.rv_top_rated_movies)
    RecyclerView rvTopRatedMovies;

    @BindView(R.id.cv_upcoming_movies)
    CardView cvUpcomingMovies;
    @BindView(R.id.rv_upcoming_movies)
    RecyclerView rvUpcomingMovies;

    @BindView(R.id.cv_popular_series)
    CardView cvPopularSeries;
    @BindView(R.id.rv_popular_series)
    RecyclerView rvPopularSeries;

    @BindView(R.id.cv_top_rated_series)
    CardView cvTopRatedSeries;
    @BindView(R.id.rv_top_rated_series)
    RecyclerView rvTopRatedSeries;

    @BindView(R.id.cv_upcoming_series)
    CardView cvUpcomingSeries;
    @BindView(R.id.rv_upcoming_series)
    RecyclerView rvUpcomingSeries;

    private MainListener parentActivity;
    private DiscoverPresenter presenter;

    public DiscoverFragment() {
        // Required empty public constructor
    }

    public static DiscoverFragment newInstance(MainListener parent) {
        DiscoverFragment fragment = new DiscoverFragment();
        fragment.parentActivity = parent;
        return fragment;
    }

    @Override
    public int getLayoutView() {
        return R.layout.fragment_discover;
    }

    @Override
    public void onViewReady(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState, View view) {
        this.initView();
        presenter.getDiscoverData();
    }

    private void initView() {
        getApplicationComponent().inject(this);
        if(parentActivity != null){
            parentActivity.setTitle(getString(R.string.discover));
            parentActivity.setCheckedItem(R.id.nav_discover);
            parentActivity.swapDrawerMode(Constantes.DRAWER_ENABLED);
        }
        presenter = new DiscoverPresenter(moviesController, seriesController, realm, this);
        rvPopularMovies.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        rvTopRatedMovies.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        rvUpcomingMovies.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));

        rvPopularSeries.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        rvTopRatedSeries.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        rvUpcomingSeries.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
    }

    @Override
    public void setPopularMoviesAdapter(MovieListing movies) {
        rvPopularMovies.setAdapter(new MovieAdapter(movies.getResults(), parentActivity, null));
        cvPopularMovies.setVisibility(View.VISIBLE);
    }

    @Override
    public void setTopRatedMoviesAdapter(MovieListing movies) {
        rvTopRatedMovies.setAdapter(new MovieAdapter(movies.getResults(), parentActivity, null));
        cvTopRatedMovies.setVisibility(View.VISIBLE);
    }

    @Override
    public void setUpcomingMoviesAdapter(MovieListing movies) {
        rvUpcomingMovies.setAdapter(new MovieAdapter(movies.getResults(), parentActivity, null));
        cvUpcomingMovies.setVisibility(View.VISIBLE);
    }

    @Override
    public void setPopularSeriesAdapter(SeriesListing series) {
        rvPopularSeries.setAdapter(new SerieAdapter(series.getResults(), parentActivity, null));
        cvPopularSeries.setVisibility(View.VISIBLE);
    }

    @Override
    public void setTopRatedSeriesAdapter(SeriesListing series) {
        rvTopRatedSeries.setAdapter(new SerieAdapter(series.getResults(), parentActivity, null));
        cvTopRatedSeries.setVisibility(View.VISIBLE);
    }

    @Override
    public void setUpcomingSeriesAdapter(SeriesListing series) {
        rvUpcomingSeries.setAdapter(new SerieAdapter(series.getResults(), parentActivity, null));
        cvUpcomingSeries.setVisibility(View.VISIBLE);
    }

    @Override
    public void onFailure(String message) {
        activity.showSnackbar(message);
    }

    @OnClick({R.id.more_pop_movies, R.id.more_top_movies, R.id.more_upc_movies,
            R.id.more_pop_series, R.id.more_top_series, R.id.more_upc_series})
    public void seeMore(View v) {
        int id = v.getId();
        switch (id) {

            case R.id.more_pop_movies: {
                activity.pushFragment(PopularMoviesFragment.newInstance(parentActivity), true, false);
                break;
            }

            case R.id.more_top_movies: {
                activity.pushFragment(TopMoviesFragment.newInstance(parentActivity), true, false);
                break;
            }

            case R.id.more_upc_movies: {
                activity.pushFragment(UpcomingMoviesFragment.newInstance(parentActivity), true, false);
                break;
            }

            case R.id.more_pop_series: {
                activity.pushFragment(PopularSeriesFragment.newInstance(parentActivity), true, false);
                break;
            }

            case R.id.more_top_series: {
                activity.pushFragment(TopSeriesFragment.newInstance(parentActivity), true, false);
                break;
            }

            case R.id.more_upc_series: {
                activity.pushFragment(UpcomingSeriesFragment.newInstance(parentActivity), true, false);
                break;
            }
        }
    }

}
