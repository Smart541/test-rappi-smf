package pe.com.smart.themoviedbtest.vistas.base;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import butterknife.ButterKnife;
import pe.com.smart.themoviedbtest.utils.dagger.componentes.AppComponent;

public abstract class BaseFragment extends Fragment {

    private static final String TAG = BaseFragment.class.getSimpleName();
    protected BaseActivity activity;

    abstract public int getLayoutView();

    abstract public void onViewReady(LayoutInflater inflater, @Nullable ViewGroup container,
                                     @Nullable Bundle savedInstanceState, View view);

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (BaseActivity) getActivity();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        setHasOptionsMenu(true);
        View view = inflater.inflate(getLayoutView(), container, false);
        ButterKnife.bind(this, view);
        onViewReady(inflater, container, savedInstanceState, view);
        return view;
    }

    public AppComponent getApplicationComponent() {
        return this.activity.getAppComponent();
    }

}
