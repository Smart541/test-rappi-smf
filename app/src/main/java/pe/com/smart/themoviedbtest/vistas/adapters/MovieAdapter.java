package pe.com.smart.themoviedbtest.vistas.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;
import pe.com.smart.themoviedbtest.R;
import pe.com.smart.themoviedbtest.modelos.movies.MovieResult;
import pe.com.smart.themoviedbtest.utils.Constantes;
import pe.com.smart.themoviedbtest.utils.ScrollRefreshInterface;
import pe.com.smart.themoviedbtest.vistas.main.MainListener;

public class MovieAdapter extends RecyclerView.Adapter<ItemViewHolder> {
    private static final String TAG = MovieAdapter.class.getSimpleName();

    private List<MovieResult> movies;
    private ScrollRefreshInterface anInterface;
    private MainListener parentActivity;

    public MovieAdapter(List<MovieResult> movies, MainListener parentActivity, ScrollRefreshInterface anInterface) {
        this.movies = movies;
        this.anInterface = anInterface;
        this.parentActivity = parentActivity;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recicler_item, parent, false);
        return new ItemViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {

        holder.item_number.setText(String.valueOf(position + 1));
        holder.item_rating.setText(String.valueOf(movies.get(position).getVoteAverage()));

        if (movies.get(position).getPosterPath() != null)
            Picasso.get().load(Constantes.URL_IMAGES + Constantes.POSTER_SIZE + movies.get(position).getPosterPath())
                    .placeholder(R.drawable.nothumbnail).into(holder.item_image);

        if ((position == movies.size() - 1) && (anInterface != null)) {
            anInterface.loadNextPage();
        }

        holder.item_bg.setOnClickListener(v -> parentActivity.showDetail(Constantes.MOVIE_DETAIL, movies.get(holder.getAdapterPosition()).getId()));

    }

    @Override
    public int getItemCount() {
        return movies.size();
    }
}
