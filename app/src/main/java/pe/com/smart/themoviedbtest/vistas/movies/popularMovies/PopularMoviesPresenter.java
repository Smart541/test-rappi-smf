package pe.com.smart.themoviedbtest.vistas.movies.popularMovies;


import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;
import pe.com.smart.themoviedbtest.api.controladores.MoviesController;
import pe.com.smart.themoviedbtest.modelos.genres.Genre;
import pe.com.smart.themoviedbtest.modelos.genres.GenreListing;
import pe.com.smart.themoviedbtest.modelos.movies.MovieListing;
import pe.com.smart.themoviedbtest.modelos.movies.MovieResult;
import pe.com.smart.themoviedbtest.utils.Constantes;
import pe.com.smart.themoviedbtest.vistas.movies.MoviesView;

public class PopularMoviesPresenter {

    private static final String TAG = PopularMoviesPresenter.class.getSimpleName();
    private MoviesController moviesController;
    private Realm realm;
    private MoviesView moviesView;
    private String currentFilter = null;

    public PopularMoviesPresenter(MoviesController moviesController, Realm realm, MoviesView moviesView) {
        this.moviesController = moviesController;
        this.realm = realm;
        this.moviesView = moviesView;
    }

    public void getMovieGenres() {
        moviesController.getMovieGenres()
                .subscribe(this::onMovieGenresSuccess, this::onMovieGenresFailure);
    }

    private void onMovieGenresSuccess(GenreListing genreListing) {
        realm.beginTransaction();
        for (Genre item : genreListing.getGenres()) {
            item.setMovie(true);
        }
        realm.insertOrUpdate(genreListing.getGenres());
        realm.commitTransaction();
        moviesView.setFilters(genreListing);
    }

    private void onMovieGenresFailure(Throwable throwable) {
        RealmResults<Genre> realmResults = realm.where(Genre.class).equalTo("isMovie", true).sort("name", Sort.ASCENDING).findAll();

        if (realmResults.size() > 0) {
            List<Genre> results = realm.copyFromRealm(realmResults);
            GenreListing list = new GenreListing();
            list.setGenres(results);
            moviesView.setFilters(list);
        } else {
            moviesView.onFailure("Failure");
        }
    }

    public void getMovies(int page, String genresFilter) {
        this.currentFilter = genresFilter;
        moviesController.getFilteredMovies(Constantes.SORTING_POPULARITY, page, null, null, 0, genresFilter)
                .subscribe(this::onPopularMoviesSuccess, this::onPopularMoviesFailure);
    }

    private void onPopularMoviesSuccess(MovieListing movieListing) {
        realm.beginTransaction();
        realm.insertOrUpdate(movieListing.getResults());
        realm.commitTransaction();
        moviesView.updateAdapter(movieListing);
    }

    private void onPopularMoviesFailure(Throwable throwable) {
        RealmResults<MovieResult> realmResults = realm.where(MovieResult.class)
                .sort("popularity", Sort.DESCENDING).findAll();

        List<MovieResult> results = new ArrayList<>();
        if (realmResults.size() > 0) {
            results = realm.copyFromRealm(realmResults);
            if (currentFilter != null && !currentFilter.equals("")) {
                List<String> filters = new ArrayList<>(Arrays.asList(currentFilter.split("\\|")));
                results = Stream.of(results).filter(result -> {
                    HashSet<String> test = new HashSet<>(filters);
                    test.retainAll(result.getGenreIds());
                    return !test.isEmpty();
                }).collect(Collectors.toList());
            }
            currentFilter = null;
        }

        MovieListing list = new MovieListing();
        list.setResults(results);
        list.setPage(1);
        list.setTotalPages(1);
        list.setTotalResults(results.size());
        moviesView.setAdapter(list);
    }

}
