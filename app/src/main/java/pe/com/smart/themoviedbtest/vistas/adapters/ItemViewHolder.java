package pe.com.smart.themoviedbtest.vistas.adapters;

import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import pe.com.smart.themoviedbtest.R;

public class ItemViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.item_image)
    ImageView item_image;

    @BindView(R.id.item_number)
    TextView item_number;

    @BindView(R.id.item_rating)
    TextView item_rating;

    @BindView(R.id.item_bg)
    RelativeLayout item_bg;

    public ItemViewHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
    }
}

