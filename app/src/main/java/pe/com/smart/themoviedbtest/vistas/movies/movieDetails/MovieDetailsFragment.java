package pe.com.smart.themoviedbtest.vistas.movies.movieDetails;

import android.os.Bundle;

import androidx.annotation.Nullable;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerFragment;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;
import com.google.android.youtube.player.YouTubePlayerView;
import com.squareup.picasso.Picasso;
import com.uncopt.android.widget.text.justify.JustifiedTextView;

import javax.inject.Inject;

import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import butterknife.BindView;
import io.realm.Realm;
import pe.com.smart.themoviedbtest.R;
import pe.com.smart.themoviedbtest.api.controladores.MoviesController;
import pe.com.smart.themoviedbtest.modelos.movies.MovieDetails;
import pe.com.smart.themoviedbtest.utils.Constantes;
import pe.com.smart.themoviedbtest.vistas.base.BaseFragment;
import pe.com.smart.themoviedbtest.vistas.main.MainListener;

public class MovieDetailsFragment extends BaseFragment implements MovieDetailsView, YouTubePlayer.OnInitializedListener {

    @Inject
    MoviesController moviesController;
    @Inject
    Realm realm;

    @BindView(R.id.movie_image)
    ImageView movie_image;
    @BindView(R.id.movie_description)
    JustifiedTextView movie_description;
    @BindView(R.id.backdrop)
    ImageView backdrop;
    @BindView(R.id.movie_title)
    TextView movie_title;
    @BindView(R.id.movie_budget)
    TextView movie_budget;
    @BindView(R.id.movie_revenue)
    TextView movie_revenue;
    @BindView(R.id.movie_release_date)
    TextView movie_release_date;
    @BindView(R.id.movie_runtime)
    TextView movie_runtime;
    @BindView(R.id.movie_tagline)
    TextView movie_tagline;
    @BindView(R.id.movie_vote_count)
    TextView movie_vote_count;
    @BindView(R.id.fragment_youtube_player)
    FrameLayout fragmentYoutubePlayer;

    private MovieDetailsPresenter presenter;
    private int movieId;
    private String videoId = null;
    private MainListener parentActivity;

    public MovieDetailsFragment() {
        // Required empty public constructor
    }

    public static MovieDetailsFragment newInstance(int movieId, MainListener parent) {
        MovieDetailsFragment fragment = new MovieDetailsFragment();
        fragment.movieId = movieId;
        fragment.parentActivity = parent;
        return fragment;
    }

    @Override
    public int getLayoutView() {
        return R.layout.fragment_movie_details;
    }

    @Override
    public void onViewReady(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState, View view) {
        this.initializeView();
        activity.toggleLoading(true);
        presenter.getMovieDetails(movieId);
    }

    private void initializeView() {
        getApplicationComponent().inject(this);
        parentActivity.setTitle(getString(R.string.load_movie_details));
        parentActivity.swapDrawerMode(Constantes.DRAWER_DISABLED);
        presenter = new MovieDetailsPresenter(moviesController, realm, this);
    }

    @Override
    public void setData(MovieDetails movie) {
        parentActivity.setTitle(movie.getTitle());
        movie_description.setText(movie.getOverview());
        movie_title.setText(movie.getOriginalTitle());
        movie_budget.setText(movie.getBudget() + " US$");
        movie_revenue.setText(movie.getRevenue() + " US$");
        movie_release_date.setText(movie.getReleaseDate());
        movie_runtime.setText(movie.getRuntime() + " " + getString(R.string.minutes));
        movie_vote_count.setText(String.valueOf(movie.getVoteAverage()));
        if (movie.getTagline().equals("")) {
            movie_tagline.setVisibility(View.GONE);
        } else {
            movie_tagline.setText(movie.getTagline());
        }
        Picasso.get().load(Constantes.URL_IMAGES + Constantes.POSTER_SIZE + movie.getPosterPath())
                .placeholder(R.drawable.nothumbnail).into(movie_image);
        Picasso.get().load(Constantes.URL_IMAGES + Constantes.BACKDROP_SIZE + movie.getBackdropPath())
                .placeholder(R.drawable.nobackdrop).fit().centerCrop().into(backdrop);

        if ((movie.getVideos() != null) && (movie.getVideos().getResults().size() > 0)) {
            int i = 0;
            while (i < movie.getVideos().getResults().size()) {
                if (movie.getVideos().getResults().get(i).getSite().equals("YouTube")) {
                    this.videoId = movie.getVideos().getResults().get(i).getKey();
                    i = movie.getVideos().getResults().size();
                }
                i++;
            }

            YouTubePlayerSupportFragment mYoutubePlayerFragment = YouTubePlayerSupportFragment.newInstance();
            mYoutubePlayerFragment.initialize(Constantes.YOUTUBE_API_KEY, this);
            FragmentManager fragmentManager = getChildFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.fragment_youtube_player, mYoutubePlayerFragment);
            fragmentTransaction.commit();
        }

        activity.toggleLoading(false);
    }

    @Override
    public void onFailure(String message) {
        activity.showSnackbar(getString(R.string.activate_internet));
        activity.toggleLoading(false);
        activity.onBackPressed();
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean wasRestored) {
        if ((!wasRestored) && (this.videoId != null)) {
            fragmentYoutubePlayer.setVisibility(View.VISIBLE);
            youTubePlayer.cueVideo(this.videoId);
        }
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
        fragmentYoutubePlayer.setVisibility(View.GONE);
    }

}
