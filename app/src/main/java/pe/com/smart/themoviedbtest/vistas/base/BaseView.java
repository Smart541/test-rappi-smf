package pe.com.smart.themoviedbtest.vistas.base;

public interface BaseView {
    void onFailure(String message);
}
