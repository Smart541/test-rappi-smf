package pe.com.smart.themoviedbtest.vistas.series.upcomingSeries;

import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;
import pe.com.smart.themoviedbtest.api.controladores.SeriesController;
import pe.com.smart.themoviedbtest.modelos.genres.Genre;
import pe.com.smart.themoviedbtest.modelos.genres.GenreListing;
import pe.com.smart.themoviedbtest.modelos.series.SeriesListing;
import pe.com.smart.themoviedbtest.modelos.series.SeriesResult;
import pe.com.smart.themoviedbtest.utils.Constantes;
import pe.com.smart.themoviedbtest.vistas.series.SeriesView;

public class UpcomingSeriesPresenter {

    private static final String TAG = UpcomingSeriesPresenter.class.getSimpleName();
    private SeriesController seriesController;
    private Realm realm;
    private SeriesView seriesView;
    private String currentFilter = null;

    public UpcomingSeriesPresenter(SeriesController seriesController, Realm realm, SeriesView seriesView) {
        this.seriesController = seriesController;
        this.realm = realm;
        this.seriesView = seriesView;
    }

    public void getSeriesGenres() {
        seriesController.getSeriesGenres()
                .subscribe(this::onSeriesGenresSuccess, this::onSeriesGenresFailure);
    }

    private void onSeriesGenresSuccess(GenreListing genreListing) {
        realm.beginTransaction();
        realm.insertOrUpdate(genreListing.getGenres());
        realm.commitTransaction();
        seriesView.setFilters(genreListing);
    }

    private void onSeriesGenresFailure(Throwable throwable) {
        RealmResults<Genre> realmResults = realm.where(Genre.class).equalTo("isMovie", false).sort("name", Sort.ASCENDING).findAll();

        if (realmResults.size() > 0) {
            List<Genre> results = realm.copyFromRealm(realmResults);
            GenreListing list = new GenreListing();
            list.setGenres(results);
            seriesView.setFilters(list);
        } else {
            seriesView.onFailure("Failure");
        }
    }

    public void getSeries(int page, String genresFilter) {
        this.currentFilter = genresFilter;
        seriesController.getFilteredSeries(Constantes.SORTING_FIRSTAIRDATE, page, null, null, 0, genresFilter)
                .subscribe(this::onUpcomingSeriesSuccess, this::onUpcomingSeriesFailure);
    }

    private void onUpcomingSeriesSuccess(SeriesListing seriesListing) {
        realm.beginTransaction();
        realm.insertOrUpdate(seriesListing.getResults());
        realm.commitTransaction();
        seriesView.updateAdapter(seriesListing);
    }

    private void onUpcomingSeriesFailure(Throwable throwable) {
        RealmResults<SeriesResult> realmResults = realm.where(SeriesResult.class)
                .sort("firstAirDate", Sort.ASCENDING).findAll();

        List<SeriesResult> results = new ArrayList<>();
        if (realmResults.size() > 0) {
            results = realm.copyFromRealm(realmResults);
            if (currentFilter != null && !currentFilter.equals("")) {
                List<String> filters = new ArrayList<>(Arrays.asList(currentFilter.split("\\|")));
                results = Stream.of(results).filter(result -> {
                    HashSet<String> test = new HashSet<>(filters);
                    test.retainAll(result.getGenreIds());
                    return !test.isEmpty();
                }).collect(Collectors.toList());
            }
            currentFilter = null;
        }

        SeriesListing list = new SeriesListing();
        list.setResults(results);
        list.setPage(1);
        list.setTotalPages(1);
        list.setTotalResults(results.size());
        seriesView.setAdapter(list);
    }

}
