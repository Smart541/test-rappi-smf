package pe.com.smart.themoviedbtest.vistas.series.seriesDetails;

import android.os.Bundle;

import androidx.annotation.Nullable;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;
import com.squareup.picasso.Picasso;
import com.uncopt.android.widget.text.justify.JustifiedTextView;

import javax.inject.Inject;

import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import butterknife.BindView;
import io.realm.Realm;
import pe.com.smart.themoviedbtest.R;
import pe.com.smart.themoviedbtest.api.controladores.SeriesController;
import pe.com.smart.themoviedbtest.modelos.series.SerieDetails;
import pe.com.smart.themoviedbtest.utils.Constantes;
import pe.com.smart.themoviedbtest.vistas.base.BaseFragment;
import pe.com.smart.themoviedbtest.vistas.main.MainListener;

public class SerieDetailsFragment extends BaseFragment implements SerieDetailsView, YouTubePlayer.OnInitializedListener {

    @Inject
    SeriesController seriesController;
    @Inject
    Realm realm;

    @BindView(R.id.serie_image)
    ImageView serie_image;
    @BindView(R.id.serie_description)
    JustifiedTextView serie_description;
    @BindView(R.id.backdrop)
    ImageView backdrop;
    @BindView(R.id.serie_title)
    TextView serie_title;
    @BindView(R.id.serie_airdate)
    TextView serie_airdate;
    @BindView(R.id.serie_seasons)
    TextView serie_seasons;
    @BindView(R.id.serie_episodes)
    TextView serie_episodes;
    @BindView(R.id.serie_status)
    TextView serie_status;
    @BindView(R.id.serie_vote_count)
    TextView serie_vote_count;
    @BindView(R.id.fragment_youtube_player)
    FrameLayout fragmentYoutubePlayer;

    private SerieDetailsPresenter presenter;
    private int serieId;
    private String videoId = null;
    private MainListener parentActivity;

    public SerieDetailsFragment() {
        // Required empty public constructor
    }

    public static SerieDetailsFragment newInstance(int serieId, MainListener parent) {
        SerieDetailsFragment fragment = new SerieDetailsFragment();
        fragment.serieId = serieId;
        fragment.parentActivity = parent;
        return fragment;
    }

    @Override
    public int getLayoutView() {
        return R.layout.fragment_serie_details;
    }

    @Override
    public void onViewReady(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState, View view) {
        this.initializeView();
        activity.toggleLoading(true);
        presenter.getSerieDetails(serieId);
    }

    private void initializeView() {
        getApplicationComponent().inject(this);
        parentActivity.setTitle(getString(R.string.load_serie_details));
        parentActivity.swapDrawerMode(Constantes.DRAWER_DISABLED);
        presenter = new SerieDetailsPresenter(seriesController, realm, this);
    }

    @Override
    public void setData(SerieDetails serie) {
        parentActivity.setTitle(serie.getName());
        serie_title.setText(serie.getOriginalName());
        serie_description.setText(serie.getOverview());
        serie_vote_count.setText(String.valueOf(serie.getVoteAverage()));
        serie_airdate.setText(serie.getLastAirDate());
        serie_seasons.setText(String.valueOf(serie.getNumberOfSeasons()) + " " + getString(R.string.season_nmbr));
        serie_episodes.setText(String.valueOf(serie.getNumberOfEpisodes()));
        serie_status.setText(serie.getStatus());
        Picasso.get().load(Constantes.URL_IMAGES + Constantes.POSTER_SIZE + serie.getPosterPath())
                .placeholder(R.drawable.nothumbnail).into(serie_image);
        Picasso.get().load(Constantes.URL_IMAGES + Constantes.BACKDROP_SIZE + serie.getBackdropPath())
                .placeholder(R.drawable.nobackdrop).fit().centerCrop().into(backdrop);

        if ((serie.getVideos() != null) && (serie.getVideos().getResults().size() > 0)) {
            int i = 0;
            while (i < serie.getVideos().getResults().size()) {
                if (serie.getVideos().getResults().get(i).getSite().equals("YouTube")) {
                    this.videoId = serie.getVideos().getResults().get(i).getKey();
                    i = serie.getVideos().getResults().size();
                }
                i++;
            }

            YouTubePlayerSupportFragment mYoutubePlayerFragment = YouTubePlayerSupportFragment.newInstance();
            mYoutubePlayerFragment.initialize(Constantes.YOUTUBE_API_KEY, this);
            FragmentManager fragmentManager = getChildFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.fragment_youtube_player, mYoutubePlayerFragment);
            fragmentTransaction.commit();
        }

        activity.toggleLoading(false);
    }

    @Override
    public void onFailure(String message) {
        activity.showSnackbar(getString(R.string.activate_internet_series));
        activity.toggleLoading(false);
        activity.onBackPressed();
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean wasRestored) {
        if ((!wasRestored) && (this.videoId != null)) {
            fragmentYoutubePlayer.setVisibility(View.VISIBLE);
            youTubePlayer.cueVideo(this.videoId);
        }
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
        fragmentYoutubePlayer.setVisibility(View.GONE);
    }

}
